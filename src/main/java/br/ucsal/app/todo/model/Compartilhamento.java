package br.ucsal.app.todo.model;

import java.util.ArrayList;
import java.util.List;

public class Compartilhamento {
    private Long id;
    private Long usuario_id;
    private Long tarefa_id;
    private String ucompartilhadologin;
    private String ucompartilhadonome;
    private Usuario Ucompartilhado;

    private Tarefa Tcompartilhada;
    public Usuario getUcompartilhado() {
        return Ucompartilhado;
    }

    public String getUcompartilhadologin() {
        String ucompartilhadologin =  Ucompartilhado.getLogin();

        return ucompartilhadologin;
    }

    public String getUcompartilhadonome() {
        String ucompartilhadonome =  Ucompartilhado.getNome();

        return ucompartilhadonome;
    }

    public void setUcompartilhado(Usuario ucompartilhado) {
        Ucompartilhado = ucompartilhado;
    }

    public Tarefa getTcompartilhada() {
        return Tcompartilhada;
    }

    public void setTcompartilhada(Tarefa tcompartilhada) {
        Tcompartilhada = tcompartilhada;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Long usuario_id) {
        this.usuario_id = usuario_id;
    }

    public Long getTarefa_id() {
        return tarefa_id;
    }

    public void setTarefa_id(Long tarefa_id) {
        this.tarefa_id = tarefa_id;
    }
}
