package br.ucsal.app.todo.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import br.ucsal.app.todo.dao.TarefaDAO;

/**
 * Servlet implementation class TaskDeleteController
 */
@WebServlet("/excluir")
public class TaskDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	
	private TarefaDAO dao = new TarefaDAO();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String index = request.getParameter("index");
		if( index != null ) {
			try {
				long id = Long.parseLong(index);
				dao.delete(id);
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
			}
		}
		response.sendRedirect("/tarefas");


	}

}
