package br.ucsal.app.todo.controller;

import br.ucsal.app.todo.dao.UsuarioDAO;
import br.ucsal.app.todo.dao.Virus;
import br.ucsal.app.todo.model.Usuario;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebServlet("/login")
public class Login extends HttpServlet {

    private static final Long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsuarioDAO dao = new UsuarioDAO();
        String usuario = request.getParameter("login");
        String password = request.getParameter("password");
        boolean passou = false;
        Usuario usuario1 = null;
        List<Usuario> usuarios = dao.listAll();
        try {
            password = Virus.toPassword(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        for (Usuario user : usuarios) {
            if (user.getLogin().equals(usuario) && user.getSenha().equals(password)) {
                usuario1 = user;
                passou = true;
                request.getSession().setAttribute("usuario", usuario1);
                break;
            }
        }

        if(passou) {

            ((HttpServletResponse) response).sendRedirect("/tarefas");
        } else {
            response.sendRedirect("/index.jsp?erro=Erro de login e password");
        }

    }


}
