package br.ucsal.app.todo.controller;

import br.ucsal.app.todo.dao.UsuarioDAO;
import br.ucsal.app.todo.dao.Virus;
import br.ucsal.app.todo.model.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebServlet("/usuarios/mudasenha")
public class UserSenhaUpdate extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        long i = Long.parseLong(id);
        String senha = request.getParameter("senhaatual");
        String senhaNova = null;
        try {
            senha = Virus.toPassword(senha);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
        UsuarioDAO dao = new UsuarioDAO();



        if (usuario.getId() == i && usuario.getSenha().equals(senha)) {
            try {
                senhaNova = Virus.toPassword(request.getParameter("novasenha"));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            usuario.setSenha(senhaNova);
            dao.update_senha(usuario);
        } else {
                Object o = (String) "senha incorreta";
                request.setAttribute("erro", o);
            System.out.println(o);
        }

        request.setAttribute("usuario", usuario);
        request.getRequestDispatcher("/usuarios.jsp").forward(request, response);
    }

}
