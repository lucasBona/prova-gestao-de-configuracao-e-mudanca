package br.ucsal.app.todo.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import br.ucsal.app.todo.dao.TarefaDAO;
import br.ucsal.app.todo.dao.UsuarioDAO;
import br.ucsal.app.todo.model.Tarefa;
import br.ucsal.app.todo.model.Usuario;


@WebServlet(urlPatterns = { "/salvar", "/editar"} )
public class TaskSaveUpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private TarefaDAO dao = new TarefaDAO();
	private UsuarioDAO udao = new UsuarioDAO();
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String index = request.getParameter("index");
		
		if( index != null ) {
			long id = Long.parseLong(index);
			Tarefa tarefa = dao.findById(id);
			request.setAttribute("tarefa",tarefa);
		}
		
		request.setAttribute("tarefas",	dao.listByDono(index));
		request.getRequestDispatcher("tarefas.jsp").forward(request, response);

		
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String id = request.getParameter("id");
		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String concluida = request.getParameter("concluida");
		
		Tarefa tarefa = new Tarefa();
		if(id!=null && !id.trim().isEmpty()) {
			long i = Long.parseLong(id);
			tarefa.setId(i);
		}
		tarefa.setTitulo(titulo);
		tarefa.setDescricao(descricao);
		tarefa.setConcluida( concluida!=null?true:false );
		Usuario dono = new Usuario();
		dono = (Usuario) request.getSession().getAttribute("usuario");

		tarefa.setDono(dono);
		
		dao.saveOrUpdate(tarefa);
		
		response.sendRedirect("/tarefas");
	}

}


