package br.ucsal.app.todo.controller;

import br.ucsal.app.todo.dao.UsuarioDAO;
import br.ucsal.app.todo.model.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/userprofile")
public class UserProfile extends HttpServlet {

    private UsuarioDAO udao = new UsuarioDAO();


    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");

        if(usuario.getAdministracao()) {
            request.setAttribute("usuarios", udao.listAll());
            request.setAttribute("usuario", usuario);
            request.getRequestDispatcher("usuarios.jsp").forward(request, response);
        } else if (!usuario.getAdministracao()) {
            request.setAttribute("usuario", usuario);
            request.getRequestDispatcher("usuarios.jsp").forward(request, response);
        } else {
            ((HttpServletResponse) response).sendRedirect("/usuarios.jsp");
        }



    }
}
