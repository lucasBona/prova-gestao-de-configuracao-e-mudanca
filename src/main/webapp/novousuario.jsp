<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="header.jsp" ></jsp:include>
    
</head>
<body>
<c:import url="/navbar.jsp" />
<div class="h-100 d-inline-block"></div>
<div class="container">
    <form method="post" action="/usuarios/salvar"  >

        <input type="hidden" name="id" >
        <div class="form-group">
            <label for="login">Login</label>
            <input type="text" class="form-control"
                   id="login" name="login" required >
        </div>
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" required
                   id="nome" name="nome">
        </div>
        <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" required
                   id="email" name="email">
        </div>
        <div class="form-group">
            <label for="senha">Senha</label>
            <input type="password" class="form-control" required
                   id="senha" name="senha">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input"
                   id="ativo" name="ativo" >
            <label class="form-check-label" for="ativo">Ativo</label>
        </div>
        <button type="submit" class="btn btn-primary" >Salvar</button>

    </form>
</div>

<jsp:include page="scripts.jsp" ></jsp:include>

</body>
</html>