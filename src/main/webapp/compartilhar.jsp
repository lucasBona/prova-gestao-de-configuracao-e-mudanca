<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="header.jsp" ></jsp:include>
    <jsp:useBean id="compartilhadocom" class="java.util.ArrayList" scope="request" />
    <jsp:useBean id="tarefa" class="br.ucsal.app.todo.model.Tarefa" scope="request" />
</head>
<body>
<c:import url="/navbar.jsp" />

<div class="container">


    <div>

        <h5>Tarefa: ${tarefa.titulo}</h5>
        <hr>
        <h6>Tarefa coimpartilhada com:</h6>
        <table class="table table-striped ">
            <tr>
                <th>Login</th>
                <th>Nome</th>
                <th colspan="2">Quantidade de usuários: ${compartilhadocom.size()} </th>
            </tr>
            <c:forEach var="task" items="${compartilhadocom}" >
                <tr>
                    <td>${task.ucompartilhadologin}</td>
                    <td>${task.ucompartilhadonome}</td>
                    <td><a href="/deletecomp?index=${task.id}">EXCLUIR</a> </td>

                </tr>
            </c:forEach>
        </table>

    </div>

</div>

<jsp:include page="scripts.jsp" ></jsp:include>

</body>
</html>